package com.jmd.ui.tab.a_mapview.sub;

import java.awt.BorderLayout;
import java.io.Serial;

import javax.annotation.PostConstruct;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jmd.browser.BrowserEngine;
import com.jmd.common.StaticVar;

@Component
public class BrowserPanel extends JPanel {

	@Serial
	private static final long serialVersionUID = -6706118310980193510L;

	@Autowired
	private BottomInfoPanel bottomInfoPanel;
	@Autowired
	private BrowserEngine browserEngine;

	@PostConstruct
	private void init() {

		this.setBorder(new TitledBorder(null, "地图", TitledBorder.LEADING, TitledBorder.TOP,
				StaticVar.FONT_SourceHanSansCNNormal_12, null));
		this.setLayout(new BorderLayout(0, 0));

		JLabel label = new JLabel("WebView初始化");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(StaticVar.FONT_SourceHanSansCNNormal_13);
		this.add(label, BorderLayout.CENTER);

		browserEngine.setParentContainer(this);
		browserEngine.init(() -> {
			bottomInfoPanel.getContentLabel().setText(browserEngine.getVersion());
		});

	}

}
