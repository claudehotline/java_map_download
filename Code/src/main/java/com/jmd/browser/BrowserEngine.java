package com.jmd.browser;

import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import org.springframework.stereotype.Component;

import com.jmd.Application;
import com.jmd.ApplicationConfig;
import com.jmd.browser.inst.ChromiumEmbeddedCefBrowserInst;
import com.jmd.browser.inst.JavaFXBrowserInst;
import com.jmd.browser.inst.TeamDevJxBrowserInst;
import com.jmd.browser.inst.base.AbstractBrowser;
import com.jmd.callback.CommonAsyncCallback;
import com.jmd.callback.JavaScriptExecutionCallback;
import com.jmd.util.CommonUtils;

@Component
public class BrowserEngine {

    private BrowserType type = CommonUtils.isWindows() ? BrowserType.CHROMIUM_EMBEDDED_CEF_BROWSER : BrowserType.TEAMDEV_JX_BROWSER;
    private AbstractBrowser browser = null;
    private JComponent parentContainer = null;

    public void init(CommonAsyncCallback callback) {
        switch (type) {
            case JAVA_FX_BROWSER -> browser = JavaFXBrowserInst.getIstance();
            case TEAMDEV_JX_BROWSER -> browser = TeamDevJxBrowserInst.getIstance();
            case CHROMIUM_EMBEDDED_CEF_BROWSER -> browser = ChromiumEmbeddedCefBrowserInst.getIstance();
            default -> {
            }
        }
        browser.create("http://localhost:" + ApplicationConfig.startPort + "/",
                () -> new SwingWorker<Void, Void>() {
                    @Override
                    protected Void doInBackground() {
                        while (true) {
                            if (Application.isStartFinish()) {
                                show();
                                callback.execute();
                                break;
                            }
                            try {
                                Thread.sleep(150);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        return null;
                    }
                }.execute());
    }

    public void changeCore(BrowserType changeType, CommonAsyncCallback callback) {
        if (this.type == changeType) {
            return;
        }
        this.type = changeType;
        this.browser.dispose(1);
        this.parentContainer.removeAll();
        this.init(callback);
    }

    private void show() {
        SwingUtilities.invokeLater(() -> {
            this.parentContainer.removeAll();
            this.parentContainer.add(browser.getContainer(), BorderLayout.CENTER);
        });
    }

    public void setParentContainer(JComponent parent) {
        this.parentContainer = parent;
    }

    public void reload() {
        this.browser.reload();
    }

    public void dispose() {
        this.browser.dispose(0);
    }

    public void clearLocalStorage() {
        this.browser.clearLocalStorage();
    }

    public void sendShared(String topic, String message) {
        this.browser.sendShared(topic, message);
    }

    public void execJS(String javaScript) {
        this.browser.execJS(javaScript);
    }

    public BrowserEngine execJSWithStringBack(String javaScript, JavaScriptExecutionCallback callback) {
        this.browser.execJSWithStringBack(javaScript, callback);
        return this;
    }

    public BrowserType getBrowserType() {
        return this.type;
    }

    public String getVersion() {
        return this.browser.getVersion();
    }

}
