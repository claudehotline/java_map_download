package com.jmd.taskfunc;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

import javax.swing.SwingWorker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.jmd.async.pool.executor.TileDownloadExecutorPool;
import com.jmd.async.pool.executor.TileMergeExecutorPool;
import com.jmd.async.pool.scheduler.IntervalTaskSchedulerPool;
import com.jmd.async.task.scheduler.DownloadMonitoringInterval;
import com.jmd.async.task.scheduler.TileMergeMonitoringInterval;
import com.jmd.entity.geo.Tile;
import com.jmd.entity.task.ErrorTileEntity;
import com.jmd.entity.task.TaskAllInfoEntity;
import com.jmd.entity.task.TaskBlockEntity;
import com.jmd.entity.task.TaskCreateEntity;
import com.jmd.entity.task.TaskInstEntity;
import com.jmd.entity.task.TaskProgressEntity;
import com.jmd.inst.DownloadAmountInstance;
import com.jmd.os.CPUMonitor;
import com.jmd.os.RAMMonitor;
import com.jmd.ui.tab.b_download.DownloadTaskPanel;
import com.jmd.ui.tab.b_download.merge.TileMergeProgressPanel;
import com.jmd.ui.tab.b_download.task.TaskStatusPanel;
import com.jmd.ui.tab.b_download.usage.CPUPercentageLinePanel;
import com.jmd.ui.tab.b_download.usage.ResourceUsagePanel;
import com.jmd.util.CommonUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TaskExecFunc {

    @Value("${download.retry}")
    private int retry;

    @Autowired
    private IntervalTaskSchedulerPool intervalTaskSchedulerPool;
    @Autowired
    private TaskStepFunc taskStep;
    @Autowired
    private CPUMonitor cpuMonitor;
    @Autowired
    private RAMMonitor ramMonitor;
    @Autowired
    private TileDownloadExecutorPool tileDownloadExecutorPool;
    @Autowired
    private TileMergeExecutorPool tileMergeExecutorPool;
    @Autowired
    private DownloadAmountInstance downloadAmountInstance;
    @Lazy
    @Autowired
    private DownloadTaskPanel downloadTaskPanel;
    @Autowired
    private TaskStatusPanel taskStatusPanel;
    @Autowired
    private ResourceUsagePanel resourceUsagePanel;
    @Autowired
    private CPUPercentageLinePanel cpuPercentageLinePanel;
    @Autowired
    private TileMergeProgressPanel tileMergeProgressPanel;

    private final DecimalFormat df1 = new DecimalFormat("#.#");
    private final DecimalFormat df2 = new DecimalFormat("#.##");
    private boolean isHandlerCancel = false;
    private SwingWorker<Void, Void> downloadWorker;

    /**
     * 创建任务
     */
    public void createTask(TaskCreateEntity taskCreate) {
        SwingWorker<Void, Void> worker = new SwingWorker<>() {
            @Override
            protected Void doInBackground() {
                downloadTaskPanel.clearConsole();
                // 显示任务信息
                downloadTaskPanel.getProgressBar().setValue(0);
                taskStatusPanel.getCurrentContentLabel().setText("正在计算下载量");
                taskStatusPanel.getMapTypeContentLabel().setText(taskCreate.getMapType());
                taskStatusPanel.getLayersContentLabel().setText(taskCreate.getZoomList().toString());
                taskStatusPanel.getSavePathContentLabel().setText(taskCreate.getSavePath());
                taskStatusPanel.getPathStyleContentLabel().setText(taskCreate.getPathStyle());
                if (taskCreate.getIsCoverExists()) {
                    taskStatusPanel.getIsCoverExistContentLabel().setText("是");
                } else {
                    taskStatusPanel.getIsCoverExistContentLabel().setText("否");
                }
                switch (taskCreate.getImgType()) {
                    case 0 -> taskStatusPanel.getImgTypeContentLabel().setText("PNG");
                    case 1 -> taskStatusPanel.getImgTypeContentLabel().setText("JPG - 低质量");
                    case 2 -> taskStatusPanel.getImgTypeContentLabel().setText("JPG - 中等质量");
                    case 3 -> taskStatusPanel.getImgTypeContentLabel().setText("JPG - 高质量");
                    default -> {
                    }
                }
                // 生成任务
                TaskAllInfoEntity taskAllInfo = taskStep.tileDownloadTaskCreate(taskCreate, (e) -> {
                    downloadTaskPanel.consoleLog(e); // 日志回调
                });
                // 保存下载任务
                saveTaskFile(taskAllInfo);
                // 显示任务信息
                taskStatusPanel.getTileAllCountContentLabel().setText(String.valueOf(taskAllInfo.getAllRealCount()));
                taskStatusPanel.getTileDownloadedCountContentLabel().setText("0");
                taskStatusPanel.getProgressContentLabel().setText("0%");
                // 开始下载
                downloadTask(taskAllInfo);
                return null;
            }

        };
        worker.execute();
    }

    /**
     * 加载任务
     */
    public void loadTask(TaskAllInfoEntity taskAllInfo) {
        SwingWorker<Void, Void> worker = new SwingWorker<>() {
            @Override
            protected Void doInBackground() {
                downloadTaskPanel.clearConsole();
                downloadTaskPanel.consoleLog("导入下载任务...");
                List<Integer> zoomList = new ArrayList<>(taskAllInfo.getEachLayerTask().keySet());
                // 显示任务信息
                taskStatusPanel.getCurrentContentLabel().setText("正在导入下载任务");
                taskStatusPanel.getMapTypeContentLabel().setText(taskAllInfo.getMapType());
                taskStatusPanel.getLayersContentLabel().setText(zoomList.toString());
                taskStatusPanel.getSavePathContentLabel().setText(taskAllInfo.getSavePath());
                taskStatusPanel.getPathStyleContentLabel().setText(taskAllInfo.getPathStyle());
                if (taskAllInfo.getIsCoverExists()) {
                    taskStatusPanel.getIsCoverExistContentLabel().setText("是");
                } else {
                    taskStatusPanel.getIsCoverExistContentLabel().setText("否");
                }
                switch (taskAllInfo.getImgType()) {
                    case 0 -> taskStatusPanel.getImgTypeContentLabel().setText("PNG");
                    case 1 -> taskStatusPanel.getImgTypeContentLabel().setText("JPG - 低质量");
                    case 2 -> taskStatusPanel.getImgTypeContentLabel().setText("JPG - 中等质量");
                    case 3 -> taskStatusPanel.getImgTypeContentLabel().setText("JPG - 高质量");
                    default -> {
                    }
                }
                taskStatusPanel.getTileAllCountContentLabel().setText(String.valueOf(taskAllInfo.getAllRealCount()));
                updateDownloadProcess(new TaskProgressEntity(0, taskAllInfo.getAllRunCount(),
                        (double) taskAllInfo.getAllRunCount() / (double) taskAllInfo.getAllRealCount()));
                // 显示日志
                for (TaskInstEntity inst : taskAllInfo.getEachLayerTask().values()) {
                    downloadTaskPanel.consoleLog(
                            inst.getZ() + "级该多边形内总数" + inst.getAllCount() + "，需要下载的总数：" + inst.getRealCount());
                }
                // 开始下载
                downloadTask(taskAllInfo);
                return null;
            }

        };
        worker.execute();
    }

    /**
     * 开始下载
     */
    public void downloadTask(TaskAllInfoEntity _taskAllInfo) {
        SwingWorker<Void, Void> worker = new SwingWorker<>() {
            @Override
            protected Void doInBackground() {
                // 下载开始
                TaskAllInfoEntity[] taskAllInfo = new TaskAllInfoEntity[1];
                taskAllInfo[0] = _taskAllInfo;
                TaskState.IS_TASKING = true;
                downloadStart();
                // 配置HTTP
                taskAllInfo[0] = taskStep.setHttpConfig(taskAllInfo[0],
                        (e) -> downloadTaskPanel.consoleLog(e));
                // 开启定时任务
                ScheduledFuture<?> progressFuture = intervalTaskSchedulerPool
                        .setInterval(new DownloadMonitoringInterval(taskAllInfo[0],
                                (progress) -> taskSecInterval(taskAllInfo[0], progress)), 1);
                // 下载地图
                taskStep.tileDownload(taskAllInfo[0], (z) -> {
                    // 层级下载开始回调
                    taskStatusPanel.getCurrentContentLabel().setText("正在下载第" + z + "级地图");
                }, (z) -> {
                    // 层级下载结束回调 - 下载错误瓦片
                    eachLayerDownloadedTileErrorCallback(taskAllInfo[0]);
                    // 层级下载结束回调 - 合并图片
                    eachLayerDownloadedTileMergeCallback(taskAllInfo[0], z);
                }, (z, name, count, xRun, yRun, success) -> {
                    // 瓦片图下载回调
                    eachTileDownloadedCallback(taskAllInfo[0], z, name, count, xRun, yRun, success);
                }, (e) -> {
                    // 日志回调
                    downloadTaskPanel.consoleLog(e);
                });
                // 下载结束
                TaskState.IS_TASKING = false;
                intervalTaskSchedulerPool.clearInterval(progressFuture);
                downloadFinish(taskAllInfo[0]);
                System.gc();
                return null;
            }

        };
        worker.execute();
        downloadWorker = worker;
    }

    /**
     * 任务每秒回调
     */
    private void taskSecInterval(TaskAllInfoEntity taskAllInfo, TaskProgressEntity progress) {
        // 显示资源使用率
        resourceUsagePanel.getThreadCountValueLabel()
                .setText(String.valueOf(tileDownloadExecutorPool.getActiveCount()));
        resourceUsagePanel.getDownloadSpeedValueLabel().setText(downloadAmountInstance.getDiffValue() + "/s");
        resourceUsagePanel.getDownloadPerSecCountValueLabel()
                .setText(String.valueOf(progress.getCurrentCount() - progress.getLastCount()));
        resourceUsagePanel.getRamUsageValueLabel().setText(ramMonitor.getUsedRam());
        downloadAmountInstance.saveLast();
        // 计算CPU使用率
        double sysLoad = cpuMonitor.getSystemCpuLoad();
        double proLoad = cpuMonitor.getProcessCpuLoad();
        resourceUsagePanel.getSystemCpuUsageValueLabel().setText(df1.format(sysLoad * 100) + "%");
        resourceUsagePanel.getProcessCpuUsageValueLabel().setText(df1.format(proLoad * 100) + "%");
        cpuPercentageLinePanel.drawCpuUsage(sysLoad, proLoad);
        if (TaskState.IS_TASKING) { // 防止任务完成后跳回上一秒的状态
            // 更新下载进度
            updateDownloadProcess(progress);
        }
        // 保存下载任务
        if (TaskState.IS_TASKING) {
            taskAllInfo.setAllRunCount(progress.getCurrentCount());
            saveTaskFile(taskAllInfo);
        } else {
            if (!this.isHandlerCancel) {
                deleteTaskFile(taskAllInfo);
            }
        }
    }

    /**
     * 瓦片图下载回调
     */
    private void eachTileDownloadedCallback(TaskAllInfoEntity taskAllInfo, int z, String name, int count,
                                            int xRun, int yRun, boolean success) {
        if (!success) {
            String key = z + "-" + xRun + "-" + yRun;
            ErrorTileEntity errorTile = new ErrorTileEntity();
            errorTile.setKeyName(key);
            errorTile.setBlockName(name);
            errorTile.setTile(new Tile(z, xRun, yRun));
            taskAllInfo.getErrorTiles().put(key, errorTile);
        }
        TaskBlockEntity block = taskAllInfo.getEachLayerTask().get(z).getBlocks().get(name);
        block.setRunCount(count);
        block.setXRun(xRun);
        block.setYRun(yRun);
        taskAllInfo.getEachLayerTask().get(z).getBlocks().put(name, block);
    }

    /**
     * 层级下载结束回调 - 下载错误瓦片
     */
    private void eachLayerDownloadedTileErrorCallback(TaskAllInfoEntity taskAllInfo) {
        if (isHandlerCancel) {
            return;
        }
        if (taskAllInfo.getErrorTiles().size() == 0) {
            return;
        }
        taskStep.tileErrorDownload(taskAllInfo, (e) -> downloadTaskPanel.consoleLog(e));
    }

    /**
     * 层级下载结束回调 - 合并图片
     */
    private void eachLayerDownloadedTileMergeCallback(TaskAllInfoEntity taskAllInfo, int z) {
        if (isHandlerCancel) {
            return;
        }
        if (z == 0) {
            return;
        }
        if (taskAllInfo.getIsMergeTile() && !taskAllInfo.getEachLayerTask().get(z).getIsMerged()) {
            taskStatusPanel.getCurrentContentLabel().setText("正在合成第" + z + "级地图");
            // 声明Mat
            TileMergeMatWrap mat = new TileMergeMatWrap();
            tileMergeProgressPanel.getPixelCountValueLabel().setText("0/0");
            tileMergeProgressPanel.getThreadCountValueLabel().setText("0");
            tileMergeProgressPanel.getMergeProgressValueLabel().setText("0.00%");
            // 合并进度监视定时器
            ScheduledFuture<?> progressFuture = intervalTaskSchedulerPool
                    .setInterval(new TileMergeMonitoringInterval(mat, (progress) -> {
                        tileMergeProgressPanel.getPixelCountValueLabel()
                                .setText(progress.getRunPixel() + "/" + progress.getAllPixel());
                        tileMergeProgressPanel.getThreadCountValueLabel()
                                .setText(String.valueOf(tileMergeExecutorPool.getActiveCount()));
                        tileMergeProgressPanel.getMergeProgressValueLabel()
                                .setText(df2.format(progress.getPerc() * 100) + "%");
                    }), 100L);
            // 开始合并
            taskStep.mergeTileImage(mat, taskAllInfo, z, (e) -> downloadTaskPanel.consoleLog(e), (e) -> {
                tileMergeProgressPanel.getPixelCountValueLabel().setText(mat.getAllPixel() + "/" + mat.getAllPixel());
                tileMergeProgressPanel.getThreadCountValueLabel().setText("0");
                tileMergeProgressPanel.getMergeProgressValueLabel().setText("100.00%");
                // 结束合并进度监视定时器
                intervalTaskSchedulerPool.clearInterval(progressFuture);
            });
            // 完成设置true
            taskAllInfo.getEachLayerTask().get(z).setIsMerged(true);
        }
    }

    /**
     * 更新下载进度
     */
    private void updateDownloadProcess(TaskProgressEntity progress) {
        taskStatusPanel.getTileDownloadedCountContentLabel().setText(String.valueOf(progress.getCurrentCount()));
        taskStatusPanel.getProgressContentLabel().setText(df2.format(100 * progress.getPerc()) + "%");
        downloadTaskPanel.getProgressBar().setValue((int) Math.round(100 * progress.getPerc()));
    }

    /**
     * 下载开始
     */
    private void downloadStart() {
        TaskState.IS_TASK_PAUSING = false;
        isHandlerCancel = false;
        downloadTaskPanel.getPauseButton().setEnabled(true);
        downloadTaskPanel.getCancelButton().setEnabled(true);
        taskStatusPanel.getCurrentContentLabel().setText("正在下载");
        downloadAmountInstance.reset();
        resourceUsagePanel.getThreadCountValueLabel()
                .setText(String.valueOf(tileDownloadExecutorPool.getActiveCount()));
        resourceUsagePanel.getDownloadSpeedValueLabel().setText("0B/s");
        resourceUsagePanel.getDownloadPerSecCountValueLabel().setText("0");
        resourceUsagePanel.getRamUsageValueLabel().setText(ramMonitor.getUsedRam());
        cpuPercentageLinePanel.clear();
        resourceUsagePanel.getSystemCpuUsageValueLabel().setText("0.0%");
        resourceUsagePanel.getProcessCpuUsageValueLabel().setText("0.0%");
    }

    /**
     * 下载结束
     */
    private void downloadFinish(TaskAllInfoEntity taskAllInfo) {
        TaskState.IS_TASK_PAUSING = false;
        downloadWorker = null;
        resourceUsagePanel.getThreadCountValueLabel()
                .setText(String.valueOf(tileDownloadExecutorPool.getActiveCount()));
        resourceUsagePanel.getDownloadSpeedValueLabel().setText("0B/s");
        resourceUsagePanel.getDownloadPerSecCountValueLabel().setText("0");
        resourceUsagePanel.getRamUsageValueLabel().setText(ramMonitor.getUsedRam());
        downloadTaskPanel.getPauseButton().setText("暂停任务");
        downloadTaskPanel.getPauseButton().setEnabled(false);
        downloadTaskPanel.getCancelButton().setEnabled(false);
        if (!isHandlerCancel) {
            int allRunCount = 0;
            for (TaskInstEntity inst : taskAllInfo.getEachLayerTask().values()) {
                for (TaskBlockEntity block : inst.getBlocks().values()) {
                    allRunCount = allRunCount + block.getRunCount();
                }
            }
            taskAllInfo.setAllRunCount(allRunCount);
            updateDownloadProcess(new TaskProgressEntity(0, taskAllInfo.getAllRunCount(),
                    (double) taskAllInfo.getAllRunCount() / (double) taskAllInfo.getAllRealCount()));
            taskStatusPanel.getCurrentContentLabel().setText("下载完成");
            downloadTaskPanel.consoleLog("下载完成");
            System.out.println("[下载完成]");
            deleteTaskFile(taskAllInfo);
        } else {
            taskStatusPanel.getCurrentContentLabel().setText("下载任务已取消");
            downloadTaskPanel.consoleLog("下载任务已取消");
            System.out.println("[下载任务已取消]");
        }
    }

    /**
     * 是否取消
     */
    public boolean isCancel() {
        return this.isHandlerCancel;
    }

    /**
     * 暂停下载任务
     */
    public void pauseTask() {
        if (TaskState.IS_TASKING && downloadWorker != null) {
            if (TaskState.IS_TASK_PAUSING) { // 继续
                TaskState.IS_TASK_PAUSING = false;
                downloadTaskPanel.getPauseButton().setText("暂停任务");
                taskStatusPanel.getCurrentContentLabel().setText("继续下载");
                downloadTaskPanel.consoleLog("继续下载任务");
            } else { // 暂停
                TaskState.IS_TASK_PAUSING = true;
                downloadTaskPanel.getPauseButton().setText("继续任务");
                taskStatusPanel.getCurrentContentLabel().setText("暂停下载");
                downloadTaskPanel.consoleLog("已暂停下载任务");
            }
        }
    }

    /**
     * 取消下载任务
     */
    public void cancelTaks() {
        if (TaskState.IS_TASKING && downloadWorker != null) {
            downloadTaskPanel.getCancelButton().setEnabled(false);
            taskStatusPanel.getCurrentContentLabel().setText("正在取消下载任务...");
            downloadTaskPanel.consoleLog("正在取消下载任务...");
            downloadWorker.cancel(true);
            this.isHandlerCancel = true;
            TaskState.IS_TASKING = false;
        }
    }

    /**
     * 保存下载任务
     */
    private void saveTaskFile(TaskAllInfoEntity taskAllInfo) {
        try {
            CommonUtils.saveObj2File(taskAllInfo, taskAllInfo.getSavePath() + "/task_info.jmd");
        } catch (IOException e) {
            log.error("Task File Save Error", e);
        }
    }

    /**
     * 删除下载任务
     */
    private void deleteTaskFile(TaskAllInfoEntity taskAllInfo) {
        File file = new File(taskAllInfo.getSavePath() + "/task_info.jmd");
        if (file.exists() && file.isFile()) {
            file.delete();
        }
    }

}
