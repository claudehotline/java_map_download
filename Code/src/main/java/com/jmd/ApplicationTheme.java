package com.jmd;

import javax.annotation.PostConstruct;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jmd.callback.CommonAsyncCallback;
import com.jmd.common.Setting;
import com.jmd.ui.AboutFrame;
import com.jmd.ui.DonateFrame;
import com.jmd.ui.LicenseFrame;
import com.jmd.ui.MainFrame;
import com.jmd.ui.MainMenuBar;
import com.jmd.ui.ProxySettingFrame;
import com.jmd.ui.tab.a_mapview.DownloadConfigFrame;
import com.jmd.ui.tab.a_mapview.DownloadPreviewFrame;

@Component
public class ApplicationTheme {

	@Autowired
	private MainMenuBar mainMenuBar;
	@Autowired
	private MainFrame mainFrame;
	@Autowired
	private AboutFrame aboutFrame;
	@Autowired
	private LicenseFrame licenseFrame;
	@Autowired
	private DonateFrame donateFrame;
	@Autowired
	private ProxySettingFrame proxySettingFrame;
	@Autowired
	private DownloadConfigFrame downloadConfigFrame;
	@Autowired
	private DownloadPreviewFrame downloadPreviewFrame;

	@PostConstruct
	private void postInit() {
		Setting setting = ApplicationSetting.getSetting();
		mainMenuBar.getThemeNameLabel().setText("Theme: " + setting.getThemeName());
	}

	public void change(String name, String clazz, CommonAsyncCallback callback) {
		SwingUtilities.invokeLater(() -> {
			try {
				UIManager.setLookAndFeel(clazz);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			mainMenuBar.getThemeNameLabel().setText("Theme: " + name);
			updateUI(callback);
			Setting setting = ApplicationSetting.getSetting();
			setting.setThemeName(name);
			setting.setThemeClazz(clazz);
			ApplicationSetting.save(setting);
		});
	}

	public void updateUI(CommonAsyncCallback callback) {
		SwingUtilities.invokeLater(() -> {
			SwingUtilities.updateComponentTreeUI(mainFrame);
			SwingUtilities.updateComponentTreeUI(aboutFrame);
			SwingUtilities.updateComponentTreeUI(licenseFrame);
			SwingUtilities.updateComponentTreeUI(donateFrame);
			SwingUtilities.updateComponentTreeUI(proxySettingFrame);
			SwingUtilities.updateComponentTreeUI(downloadConfigFrame);
			SwingUtilities.updateComponentTreeUI(downloadPreviewFrame);
			if (callback != null) {
				callback.execute();
			}
		});
	}

}
