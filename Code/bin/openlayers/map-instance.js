const polygonFillColor = "rgba(50, 138, 288, 0.3)";
const polygonStrokeColor = "rgba(0, 90, 200, 1)";

class MapInst {

    constructor(option) {
        this.mapObj = null;
        /** 地图图层 */
        let mapLayers = [];
        // 底图
        let tilesArray = layerResources.get(option.layer);
        for (let i = 0; i < tilesArray.length; i++) {
            mapLayers.push(
                new ol.layer.Tile({
                    name: tilesArray[i].name,
                    source: tilesArray[i].source,
                    zIndex: 0,
                })
            );
        }
        // 网格
        mapLayers.push(
            new ol.layer.Tile({
                name: "Grid",
                source: new ol.source.TileDebug(),
                visible: option.grid,
                zIndex: 50
            })
        );
        // 绘制层
        this.drawSource = new ol.source.Vector();
        mapLayers.push(
            new ol.layer.Vector({
                name: "Draw",
                source: this.drawSource,
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: polygonFillColor
                    }),
                    stroke: new ol.style.Stroke({
                        color: polygonStrokeColor,
                        width: 2
                    }),
                    image: new ol.style.Circle({
                        radius: 7,
                        fill: new ol.style.Fill({
                            color: '#ffcc33'
                        })
                    })
                }),
                zIndex: 200
            })
        );
        /** 实例化地图 */
        this.mapObj = new ol.Map({
            target: option.dom,
            layers: mapLayers,
            view: new ol.View({
                projection: 'EPSG:3857',
                center: ol.proj.fromLonLat([105.203317, 37.506176]),
                zoom: 4,
                maxZoom: 21,
                minZoom: 0
            }),
            interactions: [
                new ol.interaction.DragPan(),
                new ol.interaction.PinchZoom(),
                new ol.interaction.KeyboardPan(),
                new ol.interaction.KeyboardZoom(),
                new ol.interaction.MouseWheelZoom(),
                new ol.interaction.DragZoom(),
            ],
            controls: [
                new ol.control.Zoom(),
                new ol.control.ScaleLine(),
            ]
        });
        this.currentXyzName = option.layer;
        /** 缓存变量 */
        this.modifyInteraction = null;
        this.drawInteraction = null;
        this.snapInteraction = null;
        setTimeout(() => {
            this.mapObj.setSize([window.innerWidth, window.innerHeight]);
        })
    }

    getOlMap() {
        return this.mapObj;
    }

    /** 获取当前坐标类型 */
    getCurrentCoordinateType() {
        let arr = layerResources.get(this.currentXyzName);
        return arr[0].coordinateType;
    }

    /** 获取当前图层URL */
    getCurrentXyzUrlResources() {
        let urls = [];
        let arr = layerResources.get(this.currentXyzName);
        for (let i = 0; i < arr.length; i++) {
            switch (arr[i].type) {
                case "XYZ_URL":
                    urls.push({ url: arr[i].url, support: arr[i].support });
                    break;
                default:
                    urls.push({ url: [], support: arr[i].support });
                    break;
            }
        }
        return urls;
    }

    /** 获取当前图层NAME */
    getCurrentXyzName() {
        return this.currentXyzName;
    }

    /** 移除覆盖物 */
    removeOverlay(overlay) {
        this.mapObj.removeOverlay(overlay);
    }

    /** 移除普通物 */
    removeFeature(name, feature) {
        let source = this.getLayerByName(name).getSource();
        if (source.hasFeature(feature)) {
            source.removeFeature(feature);
        }
    }
    removeFeatures(name, features) {
        let source = this.getLayerByName(name).getSource();
        for (let i = 0; i < features.length; i++) {
            if (source.hasFeature(features[i])) {
                source.removeFeature(features[i]);
            }
        }
    }

    /** zoomIn */
    zoomIn() {
        this.mapObj.getView().setZoom(this.mapObj.getView().getZoom() + 1);
    }

    /** zoomOut */
    zoomOut() {
        this.mapObj.getView().setZoom(this.mapObj.getView().getZoom() - 1);
    }

    /** 设置最大Zoom */
    setMaxZoom(z) {
        this.mapObj.getView().setMaxZoom(z);
    }

    /** 根据name获取图层 */
    getLayerByName(name) {
        let layers = this.mapObj.getLayers().getArray();
        for (let i = 0; i < layers.length; i++) {
            if (name == layers[i].getProperties().name) {
                return layers[i];
            }
        }
        return null;
    }

    /** 获取图层是否显示 */
    getLayerVisibleByName(name) {
        let layer = this.getLayerByName(name);
        if (layer) {
            return layer.getVisible();
        } else {
            return false;
        }
    }

    /** 显示网格 */
    showGrid() {
        let gridLayer = this.getLayerByName("Grid");
        gridLayer.setVisible(true);
    }

    /** 关闭网格 */
    closeGrid() {
        let gridLayer = this.getLayerByName("Grid");
        gridLayer.setVisible(false);
    }

    /** 网格是否显示 */
    getGridVisible() {
        return this.getLayerVisibleByName("Grid");
    }

    /** 更换地图类型 */
    switchMapResource(name) {
        // 删除当前图层
        let currArr = layerResources.get(this.currentXyzName);
        for (let i = 0; i < currArr.length; i++) {
            this.mapObj.removeLayer(this.getLayerByName(currArr[i].name));
        }
        // 设置新图层
        let newArr = layerResources.get(name);
        for (let i = 0; i < newArr.length; i++) {
            let addLayer = new ol.layer.Tile({
                name: newArr[i].name,
                source: newArr[i].source,
                zIndex: 0,
            });
            this.mapObj.addLayer(addLayer);
        }
        this.currentXyzName = name;
    }

    /** setFitview */
    setFitview() {
        setFitViewByFeatures({
            map: this.mapObj,
            features: mapInst.getLayerByName("Draw").getSource().getFeatures(),
            padding: [32, 32, 32, 32]
        })
    }

    /** 新建多边形Feature */
    createPolygonFeature(points) {
        let pts = [];
        for (let i = 0; i < points.length; i++) {
            pts.push(ol.proj.fromLonLat([points[i].lng, points[i].lat]));
        }
        let polygon = new ol.geom.Polygon([pts]);
        let feature = new ol.Feature({ geometry: polygon });
        let style = new ol.style.Style({
            fill: new ol.style.Fill({
                color: polygonFillColor
            }),
            stroke: new ol.style.Stroke({
                color: polygonStrokeColor,
                width: 2
            }),
        });
        feature.setStyle(style);
        this.drawSource.addFeature(feature);
        return feature;
    }

    /** 绘制多边形 */
    drawPolygon(callback) {
        this.removeModifyInteraction();
        this.removeDrawInteraction();
        this.removeSnapInteraction();
        let modify = new ol.interaction.Modify({
            source: this.drawSource,
        });
        let draw = new ol.interaction.Draw({
            source: this.drawSource,
            type: "Polygon"
        });
        let snap = new ol.interaction.Snap({
            source: this.drawSource,
        });
        modify.on("modifyend", callback.modifyEnd);
        draw.on("drawend", callback.drawEnd);
        this.mapObj.addInteraction(modify);
        this.mapObj.addInteraction(draw);
        this.mapObj.addInteraction(snap);
        this.modifyInteraction = modify;
        this.drawInteraction = draw;
        this.snapInteraction = snap;
    }

    /** 绘制圆形 */
    drawCircle(callback) {
        this.removeModifyInteraction();
        this.removeDrawInteraction();
        this.removeSnapInteraction();
        let modify = new ol.interaction.Modify({
            source: this.drawSource,
        });
        let draw = new ol.interaction.Draw({
            source: this.drawSource,
            type: "Circle"
        });
        let snap = new ol.interaction.Snap({
            source: this.drawSource,
        });
        modify.on("modifyend", callback.modifyEnd);
        draw.on("drawend", callback.drawEnd);
        this.mapObj.addInteraction(modify);
        this.mapObj.addInteraction(draw);
        this.mapObj.addInteraction(snap);
        this.modifyInteraction = modify;
        this.drawInteraction = draw;
        this.snapInteraction = snap;
    }

    /** 获取绘制的覆盖物 */
    getDrawedOverlay() {
        if (this.drawInteraction != null) {
            return this.drawInteraction.getOverlay();
        }
        return null;
    }

    /** 获取修改的覆盖物 */
    getModifiedOverlay() {
        if (this.modifyInteraction != null) {
            return this.modifyInteraction.getOverlay();
        }
        return null;
    }

    /** 获取绘制层的图形 */
    getDrawedFeatures() {
        return this.getLayerByName("Draw").getSource().getFeatures();
    }

    /** 删除绘制层的图形 */
    removeDrawedFeatures() {
        this.removeFeatures("Draw", this.getDrawedFeatures());
    }

    /** 移除绘制交互 */
    removeModifyInteraction() {
        if (this.modifyInteraction != null) {
            this.mapObj.removeInteraction(this.modifyInteraction);
            this.modifyInteraction = null
        }
    }
    removeDrawInteraction() {
        if (this.drawInteraction != null) {
            this.mapObj.removeInteraction(this.drawInteraction);
            this.drawInteraction = null
        }
    }
    removeSnapInteraction() {
        if (this.snapInteraction != null) {
            this.mapObj.removeInteraction(this.snapInteraction);
            this.snapInteraction = null
        }
    }

    /** Pan */
    pan() {
        this.removeModifyInteraction();
        this.removeDrawInteraction();
        this.removeSnapInteraction();
    }

}