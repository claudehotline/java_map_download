import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class SharedService {

    public subjects = new Map<string, Subject<any>>(); // 实例
    private persistentQueue = new Map<string, Array<any>>(); // 持久化队列

    /* 订阅 */
    sub(topic: string): Observable<any> {
        if (this.subjects.get(topic) == null) {
            this.subjects.set(topic, new Subject<any>());
            let queue = this.persistentQueue.get(topic);
            if (queue != null) {
                new Observable<boolean>((observer) => { // 异步发送已持久化的消息
                    Promise.resolve().then(() => {
                        observer.next(true);
                    })
                }).subscribe(() => {
                    if (queue != null) {
                        for (let i = 0; i < queue.length; i++) {
                            let sub = this.subjects.get(topic);
                            sub && sub.next(queue[i]);
                        }
                        this.persistentQueue.delete(topic);
                    }
                })
            }
        }
        let sub = this.subjects.get(topic);
        if (sub != null) {
            return sub.asObservable();
        } else {
            return new Observable();
        }
    }

    /* 发布 */
    pub(topic: string, msg: any, opt?: { persistent?: boolean }): void {
        if (opt && opt.persistent) { // 消息持久化
            if (this.subjects.get(topic) == null) {
                if (this.persistentQueue.get(topic) == null) {
                    this.persistentQueue.set(topic, []);
                }
                let pq = this.persistentQueue.get(topic);
                pq && pq.push(msg);
            } else {
                let sub = this.subjects.get(topic);
                sub && sub.next(msg);
            }
        } else { // 消息不持久化
            let sub = this.subjects.get(topic);
            sub && sub.next(msg);
        }
    }

    /* 销毁 */
    destroy(topic: string): void {
        if (this.subjects.get(topic) != null) {
            this.subjects.delete(topic);
        }
        if (this.persistentQueue.get(topic) != null) {
            this.persistentQueue.delete(topic);
        }
    }

    /* 销毁 */
    destroys(topics: Array<string>): void {
        for (let i = 0; i < topics.length; i++) {
            this.destroy(topics[i]);
        }
    }

    /* 清空 */
    destroyAll(): void {
        this.subjects.clear();
        this.persistentQueue.clear();
    }

}