import { MapDraw } from '../map/draw/map-draw';
import { MapWrap } from '../map/draw/map-wrap';
import { GeoUtil } from '../map/geo/geo-util';
import { Point } from '../map/geo/point';
import { MapBase } from '../map/map-base';
import { Console } from '../tool/console';
import { CommonUtil } from '../util/common-util';

export const getSharedMapFunction: Function = (topic: string): Function => {
    switch (topic) {
        /** 打开/关闭控制台 */
        case "VConsoleOpenOrClose":
            return () => {
                Console.vconsoleOpenOrClose();
            };
        /** Zoom + */
        case "ZoomIn":
            return (mapBase: MapBase, res: string) => {
                mapBase.zoomIn();
            };
        /** Zoom - */
        case "ZoomOut":
            return (mapBase: MapBase, res: string) => {
                mapBase.zoomOut();
            };
        /** Pan - */
        case "Pan":
            return (mapBase: MapBase, res: string) => {
                mapBase.pan();
            };
        /** 显示网格 */
        case "GridSwitch":
            return (mapBase: MapBase, res: string) => {
                let update;
                if (mapBase.getGridVisible()) {
                    mapBase.closeGrid();
                    update = false;
                } else {
                    mapBase.showGrid();
                    update = true;;
                }
                let config = mapBase.getMapConfig();
                if (config) {
                    config.grid = update;
                    CommonUtil.setConfigCache(config);
                    mapBase.setMapConfig(config);
                }
            };
        /** 切换图层源 */
        case "switchResource":
            return (mapBase: MapBase, res: string, callback: Function) => {
                // 切换图层
                let lastType = mapBase.getCurrentCoordinateType();
                mapBase.switchMapResource(res);
                let currentType = mapBase.getCurrentCoordinateType();
                // 保存设置
                let config = mapBase.getMapConfig();
                if (config) {
                    config.layer = res;
                    CommonUtil.setConfigCache(config);
                    mapBase.setMapConfig(config);
                }
                // 检查坐标类型
                if (lastType != currentType) {
                    if (lastType == "wgs84" && currentType == "gcj02") {
                        mapBase.turnMapFeaturesFromWgs84ToGcj02();
                    } else if (lastType == "gcj02" && currentType == "wgs84") {
                        mapBase.turnMapFeaturesFromGcj02ToWgs84();
                    }
                }
                // 回调
                setTimeout(() => {
                    callback();
                });
            };
        /** 绘制类型切换 - */
        case "switchDrawType":
            return (mapBase: MapBase, res: string) => {
                mapBase.setDrawType(res);
            };
        /** 绘制 - */
        case "openDraw":
            return (mapBase: MapBase, res: string) => {
                mapBase.pan();
                mapBase.removeDrawedFeatures();
                mapBase.openDraw({
                    drawEnd: () => {
                        setTimeout(() => {
                            mapBase.removeDrawInteraction();
                        })
                    },
                    modifyEnd: () => {
                    }
                });
            };
        /** 绘制指定多边形并定位 - */
        case "drawPolygonAndPositioning":
            return (mapBase: MapBase, res: string) => {
                mapBase.pan();
                mapBase.removeDrawedFeatures();
                let blocks = JSON.parse(res);
                for (let i = 0; i < blocks.length; i++) {
                    let points: Array<Point> = [];
                    for (let j = 0; j < blocks[i].length; j++) {
                        let point = new Point(blocks[i][j].lng, blocks[i][j].lat);
                        if (mapBase.getCurrentCoordinateType() == "wgs84") {
                            points.push(GeoUtil.gcj02_To_wgs84(point));
                        } else {
                            points.push(point);
                        }
                    }
                    let feature = MapDraw.createPolygonFeature(points);
                    MapWrap.addFeature(mapBase, mapBase.drawLayerName, feature);
                }
                mapBase.setFitviewFromDrawLayer();
            };
        /** fitview - */
        case "fitview":
            return (mapBase: MapBase, res: string) => {
                mapBase.setFitviewFromDrawLayer();
            };
        /** 删除绘制 - */
        case "removeDrawedShape":
            return (mapBase: MapBase, res: string) => {
                mapBase.removeDrawedFeatures();
            };
        default:
            return () => { };
    }
}