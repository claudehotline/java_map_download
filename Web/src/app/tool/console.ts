declare var VConsole: any;

export class Console {

    private static vconsole: any;
    private static vconsoleIsOpen: boolean = false;

    public static vconsoleOpenOrClose() {
        if (!this.vconsoleIsOpen) {
            this.openVConsole();
        } else {
            this.closeVConsole();
        }
    }

    public static openVConsole() {
        if (!this.vconsoleIsOpen) {
            this.vconsole = new VConsole();
            this.vconsoleIsOpen = true;
        }
    }

    public static closeVConsole() {
        if (this.vconsoleIsOpen) {
            this.vconsole.destroy();
            this.vconsole = null;
            this.vconsoleIsOpen = false;
        }
    }

}