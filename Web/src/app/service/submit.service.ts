import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../http/http-client.service';
import { HttpMethod } from '../http/http-method';

@Injectable()
export class SubmitService {

    constructor(private http: HttpClientService) { }

    /** 提交区域下载 */
    public blockDownload(data: any): Observable<any> {
        return this.http.request({
            method: HttpMethod.POST,
            url: 'http://localhost:26737/submit/blockDownload',
            data: data
        })
    }

    /** 世界下载 */
    public worldDownload(data: any): Observable<any> {
        return this.http.request({
            method: HttpMethod.POST,
            url: 'http://localhost:26737/submit/worldDownload',
            data: data
        })
    }

}