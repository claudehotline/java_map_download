export class PointENObj {

    lng: {
        h: number,
        m: number,
        s: number,
    };
    lat: {
        h: number,
        m: number,
        s: number,
    };

    constructor(lng: { h: number, m: number, s: number }, lat: { h: number, m: number, s: number }) {
        this.lng = {
            h: lng.h,
            m: lng.m,
            s: lng.s,
        };
        this.lat = {
            h: lat.h,
            m: lat.m,
            s: lat.s,
        };
    }

}